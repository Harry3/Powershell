﻿if ((Get-WmiObject -Class Win32_OperatingSystem -ComputerName $Computer -ea 0).OSArchitecture -eq '64-bit') {           
     "64-Bit"            
   } else  {            
     "32-Bit"            
   }       