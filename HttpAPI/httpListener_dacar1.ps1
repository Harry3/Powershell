﻿# PowerShell RESTful server by Parul Jain paruljain@hotmail.com
# Version 0.1 April 4th, 2013
# 
# Use to offer services to other programs as a simple alternative to remoting and webservice technologies
# Does not require a webserver such as IIS. Works on its own!
# Single threaded; will process requests in order. If requests take long to execute and hundreds of simultaneous clients are expected
#    code needs to be re-written to create a new job per request (I will create that version in the future) 
# Can return plain text, XML, JSON, HTML, etc. XML and JSON are easy to consume from clients. JSON especially with Javascript, JQuery
# Inspired by PERL Dancer and Python Flask 
# 
# Requires PowerShell v3.0 or better (will work in v2.0 but ConvertTo-Json not available)
# 
# On windows 7, 2008R2 and 2012 run the following command once from an administratively privileged command prompt
# This allows your program to listen on port 8000. You can change the port number per your requirements 
# netsh http add urlacl url=http://+:8000/ user=domain\user

# Start of script
param(
        [string] $remoteHost = "127.0.0.1",
        [int] $port = 4242
        )

$listener = New-Object System.Net.HttpListener
$listener.Prefixes.Add('http://+:8000/') # Must exactly match the netsh command above
    

$listener.Start()
'Listening ...'
$loop = $true
while ($loop) {
    $context = $listener.GetContext() # blocks until request is received
    $request = $context.Request
    $response = $context.Response
    
    # Equivalent to 'routes' in other frameworks
    if ($request.Url -match '/date$') { # response to http://myServer:8000/date
        $response.ContentType = 'text/plain'
        $message = [System.DateTime]::Now.ToString()
    }
    
    if ($request.Url -match '/date/xml$') { # response to http://myServer:8000/date/xml
        $response.ContentType = 'text/xml'
        $hour = [System.DateTime]::Now.Hour
        $minute = [System.DateTime]::Now.Minute
        $message = "<?xml version=""1.0""?><Time><Hour>$hour</Hour><Minute>$minute</Minute></Time>"
    }

    if ($request.Url -match '/date/json$') { # response to http://myServer:8000/date/json
        $response.ContentType = 'application/json'
        $time = '' | select hour, minute
        $time.hour = [System.DateTime]::Now.Hour
        $time.minute = [System.DateTime]::Now.Minute
        $message = $time | ConvertTo-Json
    }


     ## navistart
    if ($request.Url -match '/navistart$') { # response to http://myServer:8000/date/json
      &"C:\Program Files (x86)\Navigator15\PC_Navigator\PC_Navigator.exe" --atlas='C:\ProgramData\Navigator\15.0\atlas_pcn_free.idc'  
      $response.ContentType = 'text/plain'
      $message = ' Navigation Start at: ' + [System.DateTime]::Now.ToString()
      Write-Host $request.Url
     }
     
      ## navimax = Maximize
     if ($request.Url -match '/navimin$') { 
      
      ##
      

        ## Open the socket, and connect to the computer on the specified port
        write-host “Connecting to $remoteHost on port $port”
        $socket = new-object System.Net.Sockets.TcpClient($remoteHost, $port)
        if($socket -eq $null) { return; }
        start-sleep -m 500  
        #write-host “Connected.  Press ^D followed by [ENTER] to exit.`n”

        $stream = $socket.GetStream()
        $writer = new-object System.IO.StreamWriter($stream)

        $buffer = new-object System.Byte[] 1024
        $encoding = new-object System.Text.AsciiEncoding


         ## Write their command to the remote host   
         #start-sleep -m 500  

        [string] $command = '$minimize'
 
          write-host $command
           $writer.WriteLine($command)

           $writer.Flush()
           start-sleep -m 500


        ## Close the streams
        $writer.Close()
        $stream.Close()
        ##


      $response.ContentType = 'text/plain'
      $message = ' Navigation minimized at: ' + [System.DateTime]::Now.ToString()
      Write-Host $request.Url
     }

      ## navimax = Maximize
      if ($request.Url -match '/navimax$') { 
      
      ##

        ## Open the socket, and connect to the computer on the specified port
        write-host “Connecting to $remoteHost on port $port”
        $socket = new-object System.Net.Sockets.TcpClient($remoteHost, $port)
        if($socket -eq $null) { return; }
        start-sleep -m 500  
        #write-host “Connected.  Press ^D followed by [ENTER] to exit.`n”

        $stream = $socket.GetStream()
        $writer = new-object System.IO.StreamWriter($stream)

        $buffer = new-object System.Byte[] 1024
        $encoding = new-object System.Text.AsciiEncoding


         ## Write their command to the remote host   
         #start-sleep -m 500  

        [string] $command = '$maximize'
 
          write-host $command
           $writer.WriteLine($command)

           $writer.Flush()
           start-sleep -m 500


        ## Close the streams
        $writer.Close()
        $stream.Close()
        ##


      $response.ContentType = 'text/plain'
      $message = ' Navigation maximized at: ' + [System.DateTime]::Now.ToString()
      Write-Host $request.Url
     }

     ## naviexit = exit
      if ($request.Url -match '/naviexit$') { 
      
      ##

        ## Open the socket, and connect to the computer on the specified port
        write-host “Connecting to $remoteHost on port $port”
        $socket = new-object System.Net.Sockets.TcpClient($remoteHost, $port)
        if($socket -eq $null) { return; }
        start-sleep -m 500  
        #write-host “Connected.  Press ^D followed by [ENTER] to exit.`n”

        $stream = $socket.GetStream()
        $writer = new-object System.IO.StreamWriter($stream)

        $buffer = new-object System.Byte[] 1024
        $encoding = new-object System.Text.AsciiEncoding


         ## Write their command to the remote host   
         #start-sleep -m 500  

        [string] $command = '$exit'
 
          write-host $command
           $writer.WriteLine($command)

           $writer.Flush()
           start-sleep -m 500


        ## Close the streams
        $writer.Close()
        $stream.Close()
        ##


      $response.ContentType = 'text/plain'
      $message = ' Navigation exit at: ' + [System.DateTime]::Now.ToString()
      Write-Host $request.Url
     }

     ## end Listener stop
     #This will terminate the script. Remove from production!
    if ($request.Url -match '/end$') {
      $response.ContentType = 'text/plain'
      $message = 'HTTP Listener Stop at: ' + [System.DateTime]::Now.ToString() 
      $loop = $false
     }

    [byte[]] $buffer = [System.Text.Encoding]::UTF8.GetBytes($message)
    $response.ContentLength64 = $buffer.length
    $output = $response.OutputStream
    $output.Write($buffer, 0, $buffer.length)
    $output.Close()
}

$listener.Stop()