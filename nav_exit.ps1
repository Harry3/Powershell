﻿
## Connect-Computer.ps1
## Interact with a service on a remote TCP port
param(
    [string] $remoteHost = “127.0.0.1”,
    [int] $port = 4242
     )

## Open the socket, and connect to the computer on the specified port
write-host “Connecting to $remoteHost on port $port”
$socket = new-object System.Net.Sockets.TcpClient($remoteHost, $port)
if($socket -eq $null) { return; }
start-sleep -m 500  
write-host “Connected.  Press ^D followed by [ENTER] to exit.`n”

$stream = $socket.GetStream()
$writer = new-object System.IO.StreamWriter($stream)

$buffer = new-object System.Byte[] 1024
$encoding = new-object System.Text.AsciiEncoding


 ## Write their command to the remote host   
 start-sleep -m 500  

[string] $command = '$exit'
 
  write-host $command
   $writer.WriteLine($command)

   $writer.Flush()
   start-sleep -m 500

## Close the streams
$writer.Close()
$stream.Close()
